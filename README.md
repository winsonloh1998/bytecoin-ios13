# ByteCoin iOS13 (AppBrewery Course)
A iOS application that convert bitcoin currency to various fiat currency. 

## What I Have Learned
- UIPickerViewer
- Call API

## Screenshot
![Byte Coin Screenshot](https://bitbucket.org/winsonloh1998/bytecoin-ios13/raw/d80a9a8f4165bdd8982dc2c53f33e0d639274942/Screenshot/ByteCoin.png)