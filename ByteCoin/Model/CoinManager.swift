//
//  CoinManager.swift
//  ByteCoin
//
//  Created by Angela Yu on 11/09/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import Foundation

protocol CoinManagerDelegate {
    func didUpdateCoin(_ coinManager: CoinManager, coinModel: CoinModel)
    func didFailWithError(error: Error)
}

struct CoinManager {
    
    let baseURL = "https://rest.coinapi.io/v1/exchangerate/BTC"
    let apiKey = "0AA6517E-3718-4193-8BBF-56EC85379B74"
    
    let currencyArray = ["AUD", "BRL","CAD","CNY","EUR","GBP","HKD","IDR","ILS","INR","JPY","MXN","NOK","NZD","PLN","RON","RUB","SEK","SGD","USD","ZAR"]

    var delegate: CoinManagerDelegate?
    
    func getCoinPrice(for currency: String){
        let urlString = String("\(baseURL)/\(currency)?apikey=\(apiKey)")
        performRequest(urlString: urlString)
    }
    
    func performRequest(urlString: String){
        if let url = URL(string: urlString){
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url, completionHandler: {
                (data, response, error) in
                if error != nil {
                    self.delegate?.didFailWithError(error: error!)
                    return
                }
                
                if let safeData = data {
                    if let coin = self.parseJSON(safeData: safeData){
                        self.delegate?.didUpdateCoin(self, coinModel: coin)
                    }
                }
                
            })
            
            task.resume()
        }
    }
    
    func parseJSON(safeData: Data) -> CoinModel? {
        let decoder = JSONDecoder()
        
        do{
            let decodedData = try decoder.decode(CoinModel.self, from: safeData)
            return CoinModel(rate: decodedData.rate)
        } catch {
            delegate?.didFailWithError(error: error)
            return nil
        }
    }
}
