//
//  CoinModel.swift
//  ByteCoin
//
//  Created by Daniel Wong on 10/07/2020.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct CoinModel: Decodable{
    let rate: Float
}
